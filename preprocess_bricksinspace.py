#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Script is for automatically process webpage on bricksin.space with MOC and
# prepare it for rocket list.

# Takes bricksin.space moc url from clipboard. Download webpage. Tries to guess
# the author, moc title, number of bricks, type of instructions, scale.
# Generates ID for rocket_data.md. Download main image and name it as ID.jpg.
# Generates line for rocket_data.md and copy it to clipboard.

# requires requests and pyperclip
#   pip install --user requests
#   pip install --user pyperclip

import requests
import re
import pyperclip
import sys
import os
from bs4 import BeautifulSoup   # needed for header.


# Get the link or url from clipboard
#  Test value: url = 'https://bricksin.space/little-joe-ii-qtv/'
url = pyperclip.paste()
url = url.split('#', 1)[0]
# Set header that defines user-agent. without user agent the webpage cannot be downloaded
header_text = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0',
}
# Download the webpage
r = requests.get(url, allow_redirects=True, headers=header_text)
if r.status_code != 200:
    sys.exit("cannot download webpage") 
webpage = r.content.decode(r.encoding)

# Guess title
#  re.DOTALL: Make the '.' special character match any character at all, including a newline; without this flag, '.' will match anything except a newline.
#  .*? - means any character 0 or more repetitions, NONGREEDY
#  <h1 class="insidepost">Little Joe II (QTV)</h1>
tmp = re.search('<h1 class="insidepost" >(.*?)</h1>', webpage, re.DOTALL)
if tmp:
    title = tmp.groups(1)[0]
else:
    title = 'T'
# Guess author
#  <p>   LEGO Designer:<br> <a href="https://bricksin.space/designer/adam-wilde/" rel="tag">Adam Wilde (Apollo 110)</a>					<br><br>Designed: October 2020</p>
tmp = re.search('LEGO Designer:.*?rel="tag">(.*?)</a>', webpage, re.DOTALL)
if tmp:
    author = tmp.groups(1)[0]
else:
    author = 'A'
# Guess no of bricks in the MOC
#  <p><strong>Part count:&nbsp;</strong>224 bricks, 68 lots.</p>
tmp = re.search('Part count:.*?strong>(.*?)bricks,', webpage, re.DOTALL)
if tmp:
    parts = tmp.groups(1)[0].strip()
else:
    parts = 'P'
# Scale is always 1:110 on Bricks In Space webpage:
scale = '1:110'
# Type of instructions not easy on Brick In Space webpage
files = 'io, pdf'

# Make ID
ID = 'MOC' + author.lower() + title.split(' ', 1)[0].lower()
# Remove non alphanumeric characters
ID = re.sub('[\W_]+', '', ID)

# Get image
img_path = '{}/{}.jpg'.format(os.path.expanduser('~'), ID)
#  <img decoding="async" class=" lazyloaded" src="https://bricksin.space/wp-content/uploads/2020/10/little-joe-ii-qtv_feature.png" data-src="https://bricksin.space/wp-content/uploads/2020/10/little-joe-ii-qtv_feature.png" alt="Little Joe II (QTV)">
tmp = re.search('https://bricksin.space/wp-content/uploads.*?.png', webpage, re.DOTALL)
if tmp:
    response = requests.get(tmp.group(), headers=header_text)
    if response.status_code == 200:
        with open(img_path, 'wb') as f:
            f.write(response.content)

# Make line for rocket_data.md
#   example:
#   MOCadamwildeapollo110little        | Rocket | Little Joe II (QTV) | 1:110 | 224 | Adam Wilde (Apollo 110) | io, pdf | 0 | [Bricks in Space webpage](https://bricksin.space/little-joe-ii-qtv/) |
line = '{}        | Rocket | {} | {} | {} | {} | {} | 0 | [Bricks in Space webpage]({}) |'.format(ID, title, scale, parts, author, files, url)

# Copy line to clipboard
pyperclip.copy(line)

