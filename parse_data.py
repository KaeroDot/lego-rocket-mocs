import json
import re
import subprocess
from abc import abstractmethod
from collections import Counter, OrderedDict
from datetime import date
from enum import Enum, EnumMeta
from typing import List, Dict, Optional, Union

import markdown

#  Definition of Content Delivery Network for images
#  Because the webpage is heavy on number of small images, it generates a lot
#  of hits. The GitLab allows only 1000 hits per one user (one showing of
#  webpage). The web defined as CDN has to serve as Content Delivery Network,
#  so the html page is obtained from GitLab, and images are obtained from CDN.
#  The img tag is done in such a way thet if CDN failes, the image is obtained
#  from GitLab.
#  (string will be used in regexp replacing and .format)
CDN = r'https://ik.imagekit.io/jmbzput5q6g/gitlabproxy/'

class DataException(Exception):
    pass


class RenderException(Exception):
    pass


class EntryType(Enum):
    Rocket = "rocket"
    Payload = "payload"
    Other = "other"
    FallonISS = "fallon iss"


class FileType(Enum):
    PDF = "pdf"
    IO = "io"
    LXF = "lxf"
    LDR = "ldr"
    BricklinkXML = "bricklink xml"
    Mecabricks = "mecabricks"
    IMAGE = "image"
    Video = "video"
    HTML = "html"
    DOCX = "docx"
    Unknown = "?"


class Column:
    def __init__(
            self,
            json_name: str,
            title: str,
            *,
            visible: bool = True,
            filter_input: Optional['Filter'] = None

    ) -> None:
        self.json_name = json_name
        self.title = title
        self.visible = visible
        self.filter_input = filter_input

    @abstractmethod
    def entry_to_html(self, entry: 'TableEntry') -> str:
        pass

    def filter_input_html(self, selected_values: Optional[List[str]] = None) -> Optional[str]:
        if self.filter_input is None or self.filter_input.hidden:
            return ""
        return self.filter_input.to_html(self, selected_values)

    @abstractmethod
    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> Union[str, List[Enum]]:
        pass

    def entry_to_json_value(self, entry: 'TableEntry') -> Union[str, List[str]]:
        return entry.data[self]

    def __eq__(self, other):
        return isinstance(other, Column) and self.json_name == other.json_name

    def __hash__(self):
        return hash(self.json_name)


class IDColumn(Column):

    def entry_to_html(self, entry: 'TableEntry') -> str:
        entry_id = entry.data[self]
        url = "https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#{}".format(entry_id)
        image_src = "img/{}.jpg".format(entry_id)
        # This line makes an img tag that loads an image from CDN, if fails the image is loaded from local (i.e. GitLab webpage):
        image_tag = "\n  <img\n    class=\"lazy\"\n    data-src=\"" + CDN + "{}\"\n    alt=\"{}\"\n    onerror=\"this.onerror=null;this.src='{}';\"\n  />".format(image_src, entry_id, image_src)
        return "\n  <a href=\"{}\" id=\"{}\">{}\n  </a>\n".format(url, entry_id, image_tag)

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        return md_entry.strip()


class EnumListColumn(Column):
    def __init__(
            self,
            json_name: str,
            title: str,
            enum_type: EnumMeta,
            *,
            visible: bool = True,
            filter_input: 'Filter' = None
    ) -> None:
        super().__init__(json_name, title, visible=visible, filter_input=filter_input)
        self.enum_type = enum_type

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> List[Enum]:
        try:
            return [self.enum_type(t.strip().lower()) for t in md_entry.strip(" ~").split(",")]
        except ValueError:
            raise DataException(
                "Unrecognised value \"{}\" in column \"{}\" for entry ID: \"{}\"".format(
                    md_entry, self.json_name, entry_id
                )
            )

    def entry_to_html(self, entry: 'TableEntry') -> str:
        text = ", ".join(t.value for t in entry.data[self])
        if entry.redacted:
            return "<del>{}</del>".format(text)
        return text

    def entry_to_json_value(self, entry: 'TableEntry') -> List[str]:
        return [val.value for val in entry.data[self]]


class TextColumn(Column):
    def __init__(
            self,
            json_name: str,
            title: str,
            *,
            pattern: Optional[str] = None,
            filter_input: 'Filter' = None
    ) -> None:
        super().__init__(json_name, title, filter_input=filter_input)
        self.pattern = None
        if pattern:
            self.pattern = re.compile(pattern)

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        stripped_entry = md_entry.strip()
        if stripped_entry.startswith("~~") and stripped_entry.endswith("~~"):
            stripped_entry = stripped_entry[2:-2]
        stripped_entry = stripped_entry.strip()
        # This does not need to be fullmatch, as we only care if the start of the value matches the pattern.
        if self.pattern and not self.pattern.match(stripped_entry):
            raise DataException(
                "Invalid value \"{}\" in column \"{}\" for entry ID: {}".format(
                    stripped_entry, self.json_name, entry_id
                )
            )
        return stripped_entry

    def entry_to_html(self, entry: 'TableEntry') -> str:
        if entry.redacted:
            return "<del>{}</del>".format(entry.data[self])
        return entry.data[self]


class MarkdownColumn(Column):

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        return markdown.markdown(md_entry)

    def entry_to_html(self, entry: 'TableEntry') -> str:
        if entry.redacted:
            return "<del>{}</del>".format(entry.data[self])
        return entry.data[self]


class DropdownOption:
    def __init__(self, text: str, value: str):
        self.text = text
        self.value = value


class Filter:
    def __init__(self, *, hidden: bool = False):
        self.hidden = hidden

    @abstractmethod
    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        pass

    @abstractmethod
    def to_html(self, column: Column, selected_values: Optional[List[str]] = None) -> str:
        pass


class Search(Filter):
    def __init__(self, *, hidden: bool = False):
        super().__init__(hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        value = next(iter(values or []), "")
        if value == "":
            return entries
        return [
            entry
            for entry in entries
            if value in entry.data[column]
        ]

    def to_html(self, column: Column, selected_values: Optional[List[str]] = None) -> str:
        if self.hidden:
            return ""
        selected_value = next(iter(selected_values or []), "")
        html = "<label for=\"{}\">{} </label>".format(column.json_name, column.title)
        html += "<input type=\"text\" name=\"{}\" data-column=\"{}\" class=\"search\" value=\"{}\"/>".format(
            column.json_name,
            column.json_name,
            selected_value
        )
        return html


class Dropdown(Filter):
    def __init__(self, options: List[DropdownOption], *, hidden: bool = False):
        super().__init__(hidden=hidden)
        self.options = options

    @abstractmethod
    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        pass

    def to_html(self, column: Column, selected_values: Optional[List[str]] = None) -> str:
        if not selected_values:
            selected_values = []
        if self.hidden:
            return ""
        html = "<label for=\"{}\">{} </label>".format(column.json_name, column.title)
        html += "<select name=\"{}\" data-column=\"{}\">".format(column.json_name, column.json_name)
        for option in self.options:
            html += "<option value=\"{}\" {}>{}</option>".format(
                option.value,
                "selected" if option.value in selected_values else "",
                option.text
            )
        html += "</select>"
        return html


class EntryTypesDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [DropdownOption("Any", "*")] + [DropdownOption(e.value, e.value) for e in EntryType]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        allowed_values = set(value.casefold() for value in values)
        return [
            entry
            for entry in entries
            if {entry_type.value.casefold() for entry_type in entry.data[column]}.intersection(allowed_values)
        ]


class ScaleDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [
            DropdownOption("Any", "*"),
            DropdownOption("Saturn V scale (1:110)", "1:110"),
            DropdownOption("ISS scale (1:220)", "1:220"),
            DropdownOption("Discovery scale (1:70)", "1:70"),
            DropdownOption("Minifig scale (1:29 - 1:55)", "minifig")
        ]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        return [
            entry
            for entry in entries
            if entry.data[column] in values
        ]


class FileTypesDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [DropdownOption("Any", "*")] + [DropdownOption(e.value, e.value) for e in FileType]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        allowed_values = set(value.casefold() for value in values)
        return [
            entry
            for entry in entries
            if {file_type.value.casefold() for file_type in entry.data[column]}.intersection(allowed_values)
        ]


class PriceDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [
            DropdownOption("Any", "*"),
            DropdownOption("Free", "free"),
            DropdownOption("Non-free", "non-free")
        ]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        result = []
        if "free" in values:
            result += [entry for entry in entries if entry.data[column] == "0"]
        if "non-free" in values:
            result += [entry for entry in entries if entry.data[column] != "0"]
        return result


class TableEntry:
    def __init__(
            self,
            data: Dict[Column, Union[str, List[Enum]]],
            redacted: bool
    ) -> None:
        self.data = data
        self.redacted = redacted

    @classmethod
    def from_markdown_table_row(cls, table_row: str, columns: List[Column]) -> 'TableEntry':
        row_entries = table_row.split("|")
        entry_id = row_entries[0].strip()
        cells = OrderedDict()
        for index, column in enumerate(columns):
            cells[column] = column.validate_markdown_entry(row_entries[index], entry_id)
        raw_name = row_entries[2]
        redacted = raw_name.strip().startswith("~~")
        return cls(cells, redacted)

    def to_json(self) -> Dict[str, str]:
        json_dict = {
            column.json_name: column.entry_to_json_value(self)
            for column in self.data.keys()
        }
        json_dict["redacted"] = self.redacted
        return json_dict

    def to_html_row(self) -> str:
        cells = [
            column.entry_to_html(self)
            for column in self.data.keys()
            if column.visible
        ]
        return "<tr>\n{}\n</tr>".format("\n".join("<td>{}</td>".format(cell) for cell in cells))


class Table:
    id_column = IDColumn("entry_id", "Images")
    columns = [
        id_column,
        EnumListColumn("types", "Types", EntryType, visible=False, filter_input=EntryTypesDropdown(hidden=True)),
        TextColumn("name", "Name", filter_input=Search()),
        TextColumn("scale", "Scale", pattern=r"\?|X|minifig|~?1:[0-9]+", filter_input=ScaleDropdown()),
        TextColumn("parts", "Parts", pattern=r"\?|X|[0-9]+"),
        TextColumn("designer", "Designer", filter_input=Search()),
        EnumListColumn("file_types", "Instr.", FileType, filter_input=FileTypesDropdown()),
        TextColumn("price", "Price", filter_input=PriceDropdown()),
        MarkdownColumn("links", "Links"),
        MarkdownColumn("notes", "Notes")
    ]

    def __init__(self, entries: List[TableEntry]) -> None:
        self.entries = entries

    @classmethod
    def load_from_file(cls, filename: str) -> 'Table':
        entries = []
        exceptions = []
        with open(filename, "r") as f:
            for line in f.readlines():
                if line.startswith("---|---|"):
                    continue
                if line.startswith("ID | Type(s) "):
                    continue
                if line.strip() == "":
                    continue
                try:
                    entries.append(TableEntry.from_markdown_table_row(line, cls.columns))
                except DataException as e:
                    exceptions.append(e)
        if exceptions:
            raise DataException("Data exceptions raised during parsing:\n" + "\n".join(str(e) for e in exceptions))
        entry_ids = [entry.data[cls.id_column] for entry in entries]
        counter = Counter(entry_ids)
        duplicates = [k for k, v in counter.items() if v > 1]
        if duplicates:
            raise DataException("Duplicate entry IDs: {}".format(duplicates))
        return cls(entries)

    def to_html(self, *, filters: Optional[Dict[str, List[str]]] = None) -> str:
        filtered_entries = self.filter_entries(filters)
        if not filters:
            filters = {}

        data_filters = "|".join("{}={}".format(key, ",".join(values)) for key, values in filters.items())
        filter_inputs = [
            column.filter_input_html(filters.get(column.json_name, []))
            for column in self.columns
        ]
        table_filters = " | ".join(
            x for x in filter_inputs if x
        )
        table_header = "<thead>\n<tr class=\"header\">\n{}\n</tr>\n</thead>".format(
            "\n".join(
                "<th data-column=\"{}\">{}</th>".format(column.json_name, column.title)
                for column in self.columns
                if column.visible
            )
        )
        table_body = "<tbody>\n{}\n</tbody>".format(
            "\n".join(entry.to_html_row() for entry in filtered_entries)
        )
        return "<form class=\"rocket_table\" data-filters=\"{}\">{}<table>\n{}\n{}\n</table></form>".format(
            data_filters, table_filters, table_header, table_body
        )

    def save_to_js(self, filename: str) -> None:
        with open(filename, "w") as f:
            rocket_data = json.dumps([
                entry.to_json() for entry in self.entries
            ], indent=2)
            header_comment = "//This file contains the rocket data in javascript-consumable format\n"
            js_source = "{}const rocketData = {}".format(header_comment, rocket_data)
            f.write(js_source)

    def filter_entries(self, filters: Optional[Dict[str, List[str]]]) -> List[TableEntry]:
        if not filters:
            return self.entries
        column_dict = {
            c.json_name: c for c in self.columns
        }
        entries = self.entries
        for col_name, values in filters.items():
            col = column_dict.get(col_name)
            if not col:
                raise RenderException("Unrecognised column in table filters: {}".format(col_name))
            filter_input = col.filter_input
            if not filter_input:
                raise RenderException("Column \"{}\" cannot be filtered on.".format(col_name))
            entries = filter_input.filter(entries, col, values)
        return entries


class Page:
    table_placeholder = "BUILDTABLEPLACEHOLDER"
    date_placeholder = "MAKEFILEDATEPLACEHOLDER"

    def __init__(self, file_name: str, page_html: str) -> None:
        self.file_name = file_name
        self.page_html = page_html

    @classmethod
    def load_from_file(cls, filename: str) -> 'Page':
        with open(filename, "r", encoding='utf-8') as f:
            return cls(filename, f.read())

    def replace_table_placeholders(self, table: Table) -> None:
        placeholder_re = re.compile(r"(?:<([^>]+)>|){}(?:\((.*)\))?(?:</\1>)?".format(self.table_placeholder))
        for match in placeholder_re.finditer(self.page_html):
            args = match.group(2) or ""
            args_dict: Dict[str, List[str]] = {}
            for arg in args.split("|"):
                key, val = arg.split("=")
                args_dict[key] = val.split(",")
            table_html = table.to_html(filters=args_dict)
            self.page_html = self.page_html.replace(match.group(0), table_html)

    def replace_image_placeholders(self) -> None:
        # This line makes an a and img tag that loads an image from CDN, if fails the image is loaded from local (i.e. GitLab webpage):
        tag =  "\n  <a href=\"https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#\\1\" name=\"\\1\">\n  <img\n    class=\"lazy\"\n    data-src=\"" + CDN + "img/\\1.jpg\"\n    alt=\"\\1\" \n    onerror=\"this.onerror=null;this.src='img/\\1.jpg';\"\n  />\n  </a>\n"
        self.page_html = re.sub(
            r'!<(.*?)>!',
            tag,
            self.page_html
        )

    def replace_date_placeholder(self) -> None:
        self.page_html = self.page_html.replace(self.date_placeholder, date.today().strftime("%Y.%m.%d"))

    def save(self) -> None:
        with open(self.file_name, "w") as f:
            f.write(self.page_html)


if __name__ == "__main__":
    # Parse rocket data into table
    data_table = Table.load_from_file("./rocket_data.md")
    # Run pandoc to convert markdown to html
    # pandoc:
    # -s: standalone
    # --metadata pagetitle="": adds metadata title to resulting html
    # --metadata lang="": adds metadata title to resulting html
    # -f: format is markdown github
    # --css: css style sheet
    # -o: output file
    # input file
    # made in pandoc 2.5, compiled with pandoc-types 1.17.5.4, texmath 0.11.2.2, skylighting 0.7.7
    subprocess.run(
        "pandoc -s --metadata pagetitle=\"List of LEGO models of rockets, spacecrafts and probes\" --metadata lang=en -f markdown_github+markdown_in_html_blocks --css pandoc.css -o public/index.html rockets.md",
        shell=True
    )
    # Load page
    page = Page.load_from_file("public/index.html")
    # Replace table placeholders with tables
    page.replace_table_placeholders(data_table)
    # Replace placeholders in official lego set table
    page.replace_image_placeholders()
    # Replace date placeholder with date
    page.replace_date_placeholder()
    # Save table data for javascript
    data_table.save_to_js("public/table_data.js")
    # Save file
    page.save()
