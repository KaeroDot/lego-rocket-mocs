#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Script is for automatically process webpage on rebrickable with MOC and
# prepare it for rocket list.

# Takes rebrickable moc url from clipboard. Download webpage. Tries to guess
# the author, moc title, number of bricks, type of instructions, scale.
# Generates ID for rocket_data.md. Download main image and name it as ID.jpg.
# Generates line for rocket_data.md and copy it to clipboard.

# requires requests and pyperclip
#   pip install --user requests
#   pip install --user pyperclip

import requests
import re
import pyperclip
import sys
import os

# Get the link or url from clipboard
#  Test value: url = 'https://rebrickable.com/mocs/MOC-140633/MoppeW40k/gamma-ussrfrance-gamma-x-ray-astronomical-telescope-study-1110-saturn-v-scale/#comments'
url = pyperclip.paste()
url = url.split('#', 1)[0]
# Download the webpage
r = requests.get(url, allow_redirects=True)
if r.status_code != 200:
    sys.exit("cannot download webpage") 
webpage = r.content.decode(r.encoding)

# Guess title
tmp = re.search('<h4 class="text-center mb-10">(.*?)<br>', webpage, re.DOTALL)
if tmp:
    title = tmp.groups(1)[0]
else:
    tmp = 'T'
# Guess author
tmp = re.search('class="js-hover-card" data-hover="/users/.*?/card/">(.*?)</a></h4>', webpage, re.DOTALL)
if tmp:
    author = tmp.groups(1)[0]
else:
    author = 'A'
# Guess no of bricks in the MOC
tmp = re.search('inventory">(.*?) parts</a></span>', webpage, re.DOTALL)
if tmp:
    parts = tmp.groups(1)[0]
else:
    parts = 'P'
# Guess scale
tmp = re.search('1:110', webpage, re.DOTALL)
if tmp:
    scale = '1:110'
else:
    scale = 'S'
tmp = re.search('1:220', webpage, re.DOTALL)
if tmp:
    scale = '1:220'
# Guess type of insturction file(s)
tmp = re.search('\.pdf"', webpage, re.DOTALL)
if tmp:
    files = 'pdf'
else:
    files = ''
tmp = re.search('\.io"', webpage, re.DOTALL)
if tmp:
    if len(files):
        files = ', io'
    else:
        files = 'io'

# Make ID
ID = 'MOC' + author.lower() + title.split(' ', 1)[0].lower()
# Remove non alphanumeric characters
ID = re.sub('[\W_]+', '', ID)

# Get image
img_path = '{}/{}.jpg'.format(os.path.expanduser('~'), ID)
tmp = re.search('https://cdn.rebrickable.com/media/thumbs/mocs/moc-.*?/.*?.jpg', webpage, re.DOTALL)
if tmp:
    response = requests.get(tmp.group())
    if response.status_code == 200:
        with open(img_path, 'wb') as f:
            f.write(response.content)

# Make line for rocket_data.md
#   example:
#   MOCmoppew40kisrorlvtdlex                    | Payload | ISRO RLV-TD Landing Experiment LEX | 1:110 | 64 | MoppeW40k | io | 0 | [Rebrickable](https://rebrickable.com/mocs/MOC-136291/MoppeW40k/isro-rlv-td-landing-experiment-lex-1110-saturn-v-scale/) |
line = '{}        | Rocket | {} | {} | {} | {} | {} | 0 | [Rebrickable]({}) |'.format(ID, title, scale, parts, author, files, url)

# Copy line to clipboard
pyperclip.copy(line)
