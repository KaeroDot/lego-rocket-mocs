// This file sets up the tables and click handlers and filters and stuff

function init() {
    // Find all tables, init table for them
    const rocketForms = document.querySelectorAll("form.rocket_table")
    for (const rocketForm of rocketForms) {
        initTable(rocketForm)
    }
    console.log(`Initialised ${rocketForms.length} rocket tables`)
}

function initTable(formElement) {
    // Prevent form from refreshing the page
    formElement.addEventListener("submit", (e) => e.preventDefault())
    // Parse the base filters for the table
    const filterQuery = formElement.getAttribute("data-filters")
    const tableFilters = parseFilters(filterQuery)
    // Create table object, add event listeners
    const table = new Table(formElement, tableFilters)
    for (const select of formElement.querySelectorAll("select")) {
        select.addEventListener("change",() => {
            table.updateFilters()
        })
    }
    for (const input of formElement.querySelectorAll("input.search")) {
        input.addEventListener("input", () => {
            debounce(() => table.updateFilters())
        })
    }
    for (const th of formElement.querySelectorAll("th")) {
        th.addEventListener("click", () => {
            const column = columnMap[th.getAttribute("data-column")]
            table.sort(column)
        })
        th.style.cursor = "pointer"
    }
}

function parseFilters(filterString) {
    const filters = filterString.split("|")
    const parsedFilters = []
    for(const filter of filters) {
        const [key, valueString] = filter.split("=");
        const values = valueString.split(",");
        parsedFilters.push(new Filter(columnMap[key], values))
    }
    return parsedFilters
}

let debounceInterval;
const debounce = (callback, time = 250) => {
    clearTimeout(debounceInterval);
    debounceInterval = setTimeout(() => {
        debounceInterval = null;
        callback();
    }, time);
};

class Filter {
    // A filter is a column, and a set of allowed values for the dropdown in that column.
    constructor(column, allowedValues) {
        this.column = column
        this.allowedValues = allowedValues
    }

    filter(list) {
        if (this.allowedValues.includes("*")) {
            return list
        }
        return this.column.dropdown.filter(list, this.allowedValues)
    }
}

class SearchFilter {
    constructor(column, searchValue) {
        this.column = column
        this.searchValue = searchValue
    }

    filter(list) {
        if (this.searchValue === "") {
            return list
        }
        return list.filter((item) => {
            return this.column.searchMatch(item, this.searchValue)
        })
    }
}

class Table {
    constructor(form_element, base_filters) {
        this.form_element = form_element
        this.base_filters = base_filters
        this.filters = [...base_filters]
        this.sort_by_col = IDColumn
        this.sort_by_asc = true
    }

    render() {
        // Render column header with sort order arrows
        const tr = document.createElement("tr")
        for(const column of visibleColumns) {
            const th = document.createElement("th")
            let title = column.title
            if (this.sort_by_col === column) {
                title += this.sort_by_asc ? "▴" : "▾"
            }
            th.appendChild(document.createTextNode(title))
            th.addEventListener("click", () => {
                this.sort(column)
            })
            th.style.cursor = "pointer"
            tr.appendChild(th)
        }
        this.form_element.querySelector("thead tr").replaceWith(tr)
        // Render rows in the table
        const rows = this.list_rows()
        const tbody = document.createElement("tbody")
        for(const row of rows) {
            const tr = document.createElement("tr")
            for(const column of visibleColumns) {
                const td = document.createElement("td")
                td.innerHTML = column.toHtml(row)
                tr.appendChild(td)
            }
            tbody.appendChild(tr)
        }
        this.form_element.querySelector("table tbody").replaceWith(tbody)
    }

    updateFilters() {
        // Read filter values from form
        const dropdownFilters = Array.from(this.form_element.querySelectorAll("select")).map((select) => {
            return new Filter(columnMap[select.getAttribute("data-column")], [select.value])
        }).filter((f) => f.allowedValues.length === 1 && f.allowedValues[0] !== "*")
        const searchFilters = Array.from(this.form_element.querySelectorAll("input.search")).map((input) => {
            return new SearchFilter(columnMap[input.getAttribute("data-column")], input.value)
        })
        this.filters = [...this.base_filters, ...dropdownFilters, ...searchFilters]
        console.log("Updating filters: ")
        console.log(this.filters)
        this.render()
    }

    sort(column) {
        // Sort table by specified column, if it already is, then flip order
        const old_col = this.sort_by_col
        this.sort_by_col = column
        if (old_col === column) {
            this.sort_by_asc = !this.sort_by_asc
        } else {
            this.sort_by_asc = true
        }
        console.log(`Sorting table by ${this.sort_by_col.key} ${this.sort_by_asc ? "ascending": "descending"}`)
        this.render()
    }

    list_rows() {
        // Start with all rocket data
        let filteredSorted = rocketData
        // Filter out based on dropdown filters
        for(const filter of this.filters) {
            filteredSorted = filter.filter(filteredSorted)
        }
        // Sort based on sorting options
        filteredSorted = this.sort_by_col.sort(filteredSorted)
        if(!this.sort_by_asc) {
            filteredSorted.reverse()
        }
        // Return
        return filteredSorted
    }
}


document.addEventListener("DOMContentLoaded", function () {
    console.log(`Loaded ${rocketData.length} data entries`)
    init()
})
