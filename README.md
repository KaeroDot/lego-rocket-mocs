# List of LEGO models of rockets, spacecrafts and probes.

Lists contain both official sets and MOCs that:

- are based on real rockets, spacecrafts or probes (or at least designs of real rockets, spacecrafts or probes),
- have got instructions, either free or for money.

Due to the nature of the universe, this list will never be complete and perfect, so feel free to contact me for improvements and additions, or do it yourself and submit a commit.

Thanks to all who shared their work!

# Webpage with the list
GitLab pages are used to show the List:
[https://kaerodot.gitlab.io/lego-rocket-mocs/]()

# Description of the repository
## Master file of the list
The master file with list is `rocket_data.md`. It is a markdown file (GitHub
flavor) with tables. Every line is one MOC. One line must contain following table cells separated by pipe:

    ID | table_type | name | scale | bricks | author | instructions_format | price | link | comment

where:

- `ID`: unique identifier of the MOC consisting of: MOC[author][name].
- `table_type`: Type of the table, one of:
    - `rocket` - MOC represents some rockets or spacecraft,
    - `payload` - MOC represents some payload, as is probe, station part, sattelite etc.,
    - `other` - MOC depicts transporters, launch towers, vehicles, stands etc.
- `name`: Name of the MOC as named by author.
- `scale`: Scale of the MOC (1:110, 1:80,...).
- `bricks`: Number of bricks used in MOC.
- `author`: Author's name.
- `instructions_format`: list of instruction or/and digital file formats (pdf, io, xml, docx, ...).
- `price`: Price of the MOC.
- `link`: One or more links to the MOC.
- `comment`: Any comment, or link to other related MOC.

The table is shown as is in output webpage, and is also converted into
javascript files that serve as table database for user sorting and filtering (thanks to Josh Coales).

## Webpage source
Source is in file *rockets.md*, markdown file (GitHub flavor).

## Images
MOC images for tables are scaled so the largest dimension is maximally 1000
pixels, jpg with quality 50 %. Script `resize_convert_images` can convert file
as needed. These images are in directory `public/img`. Images for comparisons are in `public/imgCOM`.

## Content Delivery Network
Because the webpage contains many images, and splitting the webpage would
impact searching of MOC, and GitLab limits 1000 hits per user at once, a
Content Delivery Network is needed. Actually *imagekit.io* CDN is used.
Definition of CDN is at the top of the python script. The CDN works
automatically, no data uploads are needed.

## Building
The webpage is built from markdown files using `pandoc`. The data from
`rocket_data.md` for javascript database is parsed by python script
`parse_data.py`. The python script also pre-process the markdown files and
fills in proper html tags with links. The script calls `pandoc` to make final
html files.
All is initiated by the Makefile, so the webpage is built using command `make`.

## Requirements
*Python 3* to run the build script and *markdown* python module, *pandoc* to
convert markdown files to html files, preferably *make* to simplify repetitive
building, *convert* and *mogrify* for automatic conversion of images.

## My workflow
- Find out new MOCs. Rebrickable can be set to send email when a new MOC of
  followed builders is released. FlosRocketBricks made his page as git
  repository, so it is easy to follow new changes. Other sources are hard to
  follow.
- Add new MOC into `rockets.md`. This means to select a new MOC ID
  (`MOC[author][name]`) and write a new table line. 
  If the MOC is at Rebrickable, one can copy its web address into the clipboard
  and run the script `preprocess_rebrickable.py`, that will create a new MOC
  line into the clipboard and save first MOC image to a home directory. Usually
  one have to fix MOC ID (and properly rename the image file), often
  also fix the instruction type (pdf, io).
- Add possible comment to the table line (last collumn). Usually I note:
    - the MOC is based on some other older MOC and add link to the older MOC,
    - the MOC is modification of some LEGO set (and add link),
    - the MOC is related to another MOC, e.g. a rocket and a launch site (and add link),
    - the MOC is part of some series etc (and add a link to all MOCs in the series).
- Prepare the MOC image:
    - Cut out unnecessary parts of the image. Keep only the MOC itself to get smaller image.
    - Convert it to jpg with 50 % quality using bash script `resize_convert_images`.
    - Rename it to `[MOC ID].jpg` and move it to the `public/img` folder.
- Compile the web using `make`.
- Commit changes using `git add --all; git commit`.
- Push changes to the remote repository using `git push`.
