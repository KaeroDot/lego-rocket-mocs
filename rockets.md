<div class="navbar">
[Top](#webtop) | [TOC](#1-table-of-contents) | [Sources](#2-main-sources-of-mocs) | [**Rockets**](#3-table-of-rockets-and-spacecrafts) | [**Payloads**](#4-table-of-satellites-and-probes-payload) | [**Fallon's ISS**](#5-iss-system-by-dan-fallon-et-all) | [**Others**](#6-table-of-launchpads-stands-etc) | [LEGO sets](#7-table-of-official-lego-sets) | [Documents](#8-documents) | [Comparisons](#9-comparisons) | [Links](#10-various-interresting-links) | [Honorary](#11-honorary-mentions) | [Changes](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/commits/master)
</div>
# List of LEGO models of rockets, spacecrafts and probes <a name="webtop"></a>
<details><summary>Show/hide introduction. **Read me. Really.**</summary>

This page is a list of MOCs that:
1. are based on **real** rockets, spacecrafts or probes (or at least designs of planned real rockets, spacecrafts or probes),
2. have got **instructions**, either free or for money. Instructions means either digital file (io, lxf, etc.) or full instructions (pdf, jpg, etc.) are available.

This page is only a guidepost, a collection of links with images. No instructions are stored here.

**Thanks to all who shared their work!**
**Thanks to Josh Coales, who made python scripts and javascripts for generating tables with filtering and sorting.**

If you are offended and you do not want your work to be included in the list, please contact me.
Contact me in this **[Eurobricks forum topic](https://www.eurobricks.com/forum/index.php?/forums/topic/173503-list-of-lego-models-of-rockets-spacecrafts-and-probes/)** or directly **[Eurobricks - Kaero](https://www.eurobricks.com/forum/index.php?/profile/167628-kaero/)**.
Due to the nature of the universe, this list will never be complete and perfect, so please contact me with ideas for improvements and additions, or join the developement.
This page is on [GitLab](https://gitlab.com/KaeroDot/lego-rocket-mocs) and the [README](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/blob/master/README.md) got details on the page and how to contribute.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. The license does NOT cover images. Images are property of MOC authors.

Tables can be sorted by clicking on the heading. If you need link to a particular MOC or set, click on the image and copy address from the address bar (URL bar) of your browser.

Last updated: MAKEFILEDATEPLACEHOLDER.
</details>

## 1. Table of contents
<details><summary>Show/hide</summary>
1. [This Table of contents](#1-table-of-contents)
2. [Main sources of MOCs](#2-main-sources-of-mocs)
    1. [Bricks in space webpage](#21-bricks-in-space-webpage)
    2. [Bricks in space books](#22-bricks-in-space-books)
    3. [Facebook group Bricks in Space](#23-facebook-group-bricks-in-space)
    4. [Alexandru Bauer's google drive](#24-alexandru-bauers-google-drive)
    5. [Facebook group Lego Rocket Collection and its website](#25-facebook-group-lego-rocket-collection-and-its-website)
    6. [Rebrickable](#26-rebrickable)
    7. [SpaceXplorer website](#27-spacexplorer-website)
    8. [Bricklink](#28-bricklink)
    9. [Reddit](#29-reddit)
    10. [WillM's google drive](#210-willms-google-drive)
    11. [Mecabricks](#211-mecabricks)
    12. [Eurobricks](#212-eurobricks)
    13. [Lego Ideas](#213-lego-ideas)
    14. [MOCpages](#214-mocpages)
    15. [BRICKmeetsDESGIN](#215-brickmeetsdesign)
    16. [Etsy](#216-etsy)
    17. [Bricksat - Satelliten aus Darmstadt](#217-bricksat---satelliten-aus-darmstadt)
    18. [Brickmania](#218-brickmania)
    19. [Brick Space Agency](#219-brick-space-agency)
    20. [FlosRocketBricks](#220-flosrocketbricks)
    21. [Rocket Bricks](#221-rocket-bricks)
    22. [Aloha Bricks](#222-aloha-bricks)
3. [Table of rockets and spacecrafts](#3-table-of-rockets-and-spacecrafts)
4. [Table of satellites and Probes (payload)](#4-table-of-satellites-and-probes-payload)
5. [ISS system by Dan Fallon et all.](#5-iss-system-by-dan-fallon-et-all)
6. [Table of launchpads, stands, etc.](#6-table-of-launchpads-stands-etc)
7. [Table of official LEGO sets](#7-table-of-official-lego-sets)
8. [Documents](#8-documents)
    1. [ISS by Olivier Rt](#81-iss-by-olivier-rt)
    2. [ISS by Riccardo Russo et al.](#82-iss-by-riccardo-russo-et-al)
    3. [Rudi Landmann's modding notes on KingsKnight's shuttle](#83-rudi-landmanns-modding-notes-on-kingsknights-shuttle)
    4. [Stickers and decals](#84-stickers-and-decals)
    5. [Overview of Energia family](#85-overview-of-energia-family)
    6. [Apollo Moon mission chart](#86-apollo-moon-mission-chart)
    7. [Dimensions of cylinders](#87-dimensions-of-cylinders)
    8. [LEGO cutaway Saturn V](#88-lego-cutaway-saturn-v)
    9. [Guides with modifications for set 21321](#89-guides-with-modifications-for-set-21321)
   10. [Soyuz 7K development tree](#810-soyuz-7k-development-tree)
9. [Comparisons](#9-comparisons)
    1. [A4, 1:110 scale](#91-a4-1110-scale)
    2. [Space Shuttle](#92-space-shuttle)
    3. [Launch Umbilical Towers, 1:110 scale](#93-launch-umbilical-towers-1110-scale)
    4. [Soyuz](#94-soyuz)
    5. [Falcon and Falcon heavy](#95-falcon-and-falcon-heavy)
    6. [Titan II Gemini Launch Vehicle](#96-titan-ii-gemini-launch-vehicle)
    7. [H-II Transfer Vehicle](#97-h-ii-transfer-vehicle)
    8. [Saturn IB](#98-saturn-ib)
10. [Various interresting links](#10-various-interresting-links)
11. [Honorary mentions](#11-honorary-mentions)
12. [History of changes](#12-history-of-changes)
</details>

## 2. Main sources of MOCs
<details><summary>Show/hide sources.</summary>
The lists contain projects I have found on following sites: 

### 2.1. Bricks in space webpage
  (last update 22.1.2025)

  [Bricks in Space webpage](https://bricksin.space/) was created by the most prolific MOC designers (of real rockets), freely sharing their own work. This is the main and first web to look for sorted MOC building instructions with descriptions.

### 2.2. Bricks in space books
  (last update 20.4.2021)

  [Bricks in space, Modelling Spaceflight with Lego](http://www.ametria.org/lego/) is a series of wonderful books prepared by Wolfram Broszies and can be download for free. Books offer description, history and technical data for many rockets and space ships with many pictures and building instructions for many models. Worth printing.
  - First volume contains models: [A4](#MOCmarkbalderramaa4), [V2 launch platoon](#MOCwolframbrosziesv2launchplatoon), [Redstone](#MOCwolframbrosziesredstone), [Vanguard](#MOCbenjaminunisvanguard), [Scout X-1](#MOCbenjaminunisscoutx1), [Juno I](#MOCeifflemanjuno1), [Juno II](#MOCeifflemanjunoii), [Thor-Delta D](#MOCeifflemanthordelta), [Atlas-Agena](#MOCeifflemanagenad), [Little Joe](#MOCwolframbroszieslittlejoe), [Mercury Redstone](#MOCeifflemanmercuryredstone), [Launch Pad 5](#MOCeifflemanmercuryredstonelaunchset), [Atlas Mercury](#MOCeifflemanmercuryatlas), [Titan Gemini](#MOCeifflemangeminititan), [Rocket Garden](#MOCeifflemanrocketgarden).
  - Second volume contains models: [Saturn I](#MOCdavidwells44saturn1series), [Saturn Ib](#MOCdavidwells44saturn1bseries), [Launch Complex 34-37](#MOCdavidwellinglaunchcomplex34), [The Milkstool](#MOCdavidwellingmilkstool), [Little Joe II](#MOCeifflemanlittlejoeii), [Saturn V](#SET21309-1), [Astrovan](#MOCeifflemanastrovan), [M-113 Astronaut Rescue Vehicle](#MOCeifflemanm113), [Lunar Rover](#MOCXlunarrover), [Rescue Helicopter 66](#MOCkaeroapollorecovery), [Mobile Quarantaine Facility](#MOCeifflemanmqf), [Skylab](#MOCeifflemanskylab), [The Crawler](#MOCeifflemancrawler).
  - Third volume contains models: [Launch Umbilical Tower](#MOCnathanrlut).

### 2.3. Facebook group Bricks in Space
  (last update 19.1.2024)

  [Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/) is regularly visited by majority of real space MOC designers. The file section of this group is the largest archive of digital files and instructions, unfortunately unsorted. Therefore a Google drive was set up by Alexandru Bauer (see next item).

### 2.4. Alexandru Bauer's google drive
  (last update 27.1.2024), 

  [Google drive by A. Bauer](https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w) contains numerous MOCs, mostly found also at Facebook group Bricks in Space, Bricks in Space webpage or Rebrickable. The drive is maintained from time to time, yet slightly messy.

### 2.5. Facebook group Lego Rocket Collection and its website
  (last update 16.4.2021)

  [Facebook group Lego Rocket Collection](https://www.facebook.com/groups/178082023364995/) is a facebook private group about a very large collection of rockets built in scale 1:110 by Nicolas Riveau. You can [support Nicolas using tipeee](https://en.tipeee.com/legorocketcollection/). Recently a webpage [legorocketcollection.com](https://new.legorocketcollection.com) presenting the collection was started.

### 2.6. Rebrickable
  (last update 17.01.2025)

  [Rebrickable](https://rebrickable.com/) now contains probably the largest collection of space MOCs. It is very convenient web page that can manage your parts, builds and MOCs. First source to go.

### 2.7. SpaceXplorer website
  (last update 26.5.2022)

  [SpaceXplorer MOCs](https://spacexplorer-mocs.my-free.website) is a web page with MOC's by SpaceXplorer. MOCs are also on [Rebrickable](https://rebrickable.com/users/SpaceXplorer%20MOCs/mocs/). Some digital files are also available at [SpaceXplorer's google drive](https://drive.google.com/drive/folders/1kB5w1oFoZmOfLH49lAxYBJ-3pf5EpW5z).

### 2.8. Bricklink
  (last update 28.2.2021)

  [Bricklink](https://www.bricklink.com/) contains a gallery with several related MOCs.

### 2.9. Reddit
  (last update 20.4.2021)

  [legoRockets at Reddit](https://www.reddit.com/r/legoRockets/) is a discussion forum with occasional instructions.

### 2.10. WillM's google drive
  (last update 20.4.2021)

  [Google drive by WillM](https://drive.google.com/drive/folders/1Qd-fQ0YeCxj_q_PXCCZb0ciwsnvjvgnc) contains some instructions.

### 2.11. Mecabricks
  (last update 4.10.2019)

  [Mecabricks](https://www.mecabricks.com/) contains few related MOCs. One can see the models, unfortunately using Mecabricks to build something will be only for experienced builders because export is not possible.

### 2.12. Eurobricks
  (last update 8.1.2021)

  [Eurobricks forum](https://www.eurobricks.com/) is is excellent forum for AFOLs, unfortunately only few rocket builders post there. 

### 2.13. Lego Ideas
  (last update 15.3.2021)

  [Lego Ideas](https://ideas.lego.com/) hosts many space MOCs but only a few contributors share the instructions.

### 2.14. ~~MOCpages~~
  (last update 9.10.2019)

  [MOCpages](http://www.moc-pages.com) contained a lot of MOCs, but mostly without any digital file nor instructions. MOCpages have been deleted by owner.

### 2.15. BRICKmeetsDESIGN
  (last update 11.6.2021)

  [BRICKmeetsDESIGN webpage with creations by Kevin Huang.](https://www.brickmeetsdesign.com/)

### 2.16. Etsy
  Several authors sell their MOCs on [Etsy](https://www.etsy.com/).

### 2.17. Bricksat - Satelliten aus Darmstadt
  (last update 3.2.2024)

  [Bricksat](https://bricksat.onlineweb.shop) sells bricks for satellite MOCs of his or other's
  designs.

### 2.18. Brickmania
  (last update 22.5.2022)

  [Brickmania](https://www.brickmania.com) sells many custom sets, minifigs, stickers and instructions by various designers.

### 2.19. Brick Space Agency
  (last update 22.5.2022)

  [Brick Space Agency](https://www.brickspaceagency.com/) contains various MOCs of various authors.

### 2.20. FlosRocketBricks
  (last update - 18.1.2025)

  [FlosRocketBricks](https://flosrocketbricks.com) contains various MOCs designed by
  Florentin (aka SkySaac on Rebrickable). The web is a categorized and tagged list
  of MOCs with links to instructions on Bricksafe web. Whole web is kept as git
  repository on [Github](https://github.com/fjmoeller/flosrocketbricks).

### 2.21. Rocket Bricks
  (last update - 1. 1. 2024)

  [Rocket Bricks](https://rocketbricks.space) contains MOCs designed and sold by 0rig0.

### 2.22. Aloha Bricks
  (last update - 21. 1. 2025)

  [Facebook group Aloha Bricks](https://www.facebook.com/profile.php?id=100081895879529) contains several MOCs to buy.

</details>

## 3. Table of rockets and spacecrafts
BUILDTABLEPLACEHOLDER(types=Rocket)

## 4. Table of satellites and probes (payload)
BUILDTABLEPLACEHOLDER(types=Payload)

## 5. ISS system by Dan Fallon et all.
This project is so large a separate table is needed.
BUILDTABLEPLACEHOLDER(types=Fallon ISS)

## 6. Table of launchpads, stands, etc.
BUILDTABLEPLACEHOLDER(types=Other)

## 7. Table of official LEGO sets
Images | ID |  Name                   | Year | Scale     | Parts  | Designer | Links | Notes
---|---|---|---|---|---|---|---|---
!<SET7467-1>!   | 7467-1        | International Space Station | 2003 | ? | 162 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38859#T=P) | 
!<SET7468-1>!   | 7468-1        | Saturn V Moon Mission | 2003 | ? | 178 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=7468-1&name=Saturn V Moon Mission&category=[Discovery]#T=P) | 
!<SET7469-1>!   | 7469-1        | Mission to Mars | 2003 | ? | 417 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38850#T=P) | 
!<SET7470-1>!   | 7470-1        | Space Shuttle Discovery | 2003 | ? | 826 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=7470-1#T=P) | 
!<SET7471-1>!   | 7471-1        | Mars Exploration Rover | 2003 | ? | 867 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38856#T=P) | 
!<SET10029-1>!  | 10029-1       | Lunar Lander | 2003 | ? | 468 | LEGO | [Rebrickable](https://rebrickable.com/sets/10029-1/lunar-lander/) | 
!<SET6544-1>!   | 6544-1        | Shuttle Transcon 2 | 1995 | ? | 317 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=5143&fbclid=IwAR1-KxhRq06QVHPhw8kNm4xeE4lijACfYsN4-etb5Bg-n5OKvn9YvUr-5Ro#T=S&O=%7B%22iconly%22:0%7D) |
!<SET8480-1>!   | 8480-1        | Space Shuttle | 1996 | ? | 1366 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=8480-1) | 
!<SET10213-1>!  | 10213-1       | Shuttle Adventure | 2010 | ? | 1203 | LEGO | [Rebrickable](https://rebrickable.com/sets/10213-1/shuttle-adventure) |
!<SET10231-1>!  | 10231-1       | Shuttle Expedition | 2011 | ? | 1230 | LEGO | [Rebrickable](https://rebrickable.com/sets/10231-1/shuttle-expedition/) |
!<SET21101-1>!  | 21101-1       | Hayabusa | 2012 | ? | 368 | Daisuke Okubo  (LEGO Ideas nick: daisuke)| [Rebrickable](https://rebrickable.com/sets/21101-1/hayabusa/) | Origin: LEGO Cuusoo, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/5dfdb4a5-ea65-43bd-b671-f7c3cb125b5c), 
!<SET21104-1>!  | 21104-1       | Mars Science Laboratory Curiosity Rover | 2014 | ? | 295 | Perijove | [Rebrickable](https://rebrickable.com/sets/21104-1/mars-science-laboratory-curiosity-rover/) | Origin: LEGO Cuusoo,  [original MOC on Rebrickable](#MOCperijovecuriosity)
!<SET21309-1>!  | 21309-1       | NASA Apollo Saturn V | 2017 | 1:110 | 1969 | whatsuptoday and saabfan | [Rebrickable](https://rebrickable.com/sets/21309-1/nasa-apollo-saturn-v/) | Origin: LEGO Ideas, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/3519b723-c59a-43b4-81be-eeb99b631627)
!<SET31091-1>!  | 31091-1       | Shuttle Transporter | 2019 | mixed | 341 | LEGO | [Rebrickable](https://rebrickable.com/sets/31091-1/shuttle-transporter/) |
!<SET10266-1>!  | 10266-1       | NASA Apollo 11 Lunar Lander | 2019 | 1:35 | 1087 | LEGO | [Rebrickable](https://rebrickable.com/sets/10266-1/nasa-apollo-11-lunar-lander/) | 
!<SET21312-1>!  | 21312-1       | Women of NASA | 2017 | ? | 231 | LEGO Ideas nick: 20tauri | [Rebrickable](https://rebrickable.com/sets/21312-1/women-of-nasa/) | Origin: LEGO Ideas, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/388ddbe3-2f0a-42fb-9f54-93bf3b5f4fe9)
!<SET21321-1>!  | 21321-1       | International Space Station | 2020 | 1:220 - 1:230 | 864 | LEGO Ideas nick: 20tauri | [Rebrickable](https://rebrickable.com/sets/21321-1/international-space-station/) | Origin: LEGO Ideas, 10 Year anniversary fan vote, [original MOC on LEGO Ideas](https://ideas.lego.com/challenges/5fa4eb3f-1e98-47d7-abbc-fdc2a29b79c3/application/2ae74ed1-0c39-4e4b-8862-06409fb6c7a4)
!<SET10283-1>!  | 10283-1       | NASA Space Shuttle Discovery | 2021 | 1:70 | 2354 | LEGO | [Rebrickable](https://www.bricklink.com/v2/catalog/catalogitem.page?S=10283-1) | [Digital file by Kurtis Millette on Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/permalink/1177598505978732/)
!<SET5006744-1>!| 5006744-1     | Ulysses Space Probe | 2021 | 1:70 | 236 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=5006744-1&name=Ulysses%20Space%20Probe&category=%5BPromotional%5D%5BSpace%5D#T=S&O=%7B%22iconly%22:0%7D) | VIP Reward.
!<SET31117-1>!  | 31117-1       | Space Shuttle Adventure | 2021 | mixed | 482 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=31117-1#T=S&O=%7B%22iconly%22:0%7D) |
!<SET42158-1>!  | 42158-1       | NASA Mars Rover Perseverance | 2023 | ? | 1132 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=42158-1#T=S&O=%7B%22iconly%22:0%7D) |
!<SET31134-1>!  | 31134-1       | Space Shuttle | 2023 | ? | 144 | LEGO | [Rebrickable](https://rebrickable.com/sets/31134-1/space-shuttle/) |
!<SET11976-1>!  | 11976-1       | Spaceship | 2023 | ? | 45 | LEGO | [Rebrickable](https://rebrickable.com/sets/11976-1/spaceship/) |
!<SET30682-1>!  | 30682-1       | NASA Mars Rover Perseverance | 2024 | ? | 83 | LEGO | [Rebrickable](https://rebrickable.com/sets/30682-1/nasa-mars-rover-perseverance/) |
!<SET10341-1>!  | 10341-1       | NASA Artemis Space Launch System | 2024 | 1:160 | 3601 | LEGO | [Rebrickable](https://rebrickable.com/sets/10341-1/nasa-artemis-space-launch-system/) |
!<SET31152-1>!  | 31152-1       | Space Astronaut | 2024 | ? | 647 | LEGO | [Rebrickable](https://rebrickable.com/sets/31152-1/space-astronaut/) |
!<SET42182-1>!  | 42182-1       | NASA Apollo Lunar Roving Vehicle - LRV | 2024 | ? | 1913 | LEGO | [Rebrickable](https://rebrickable.com/sets/42182-1/nasa-apollo-lunar-roving-vehicle-lrv/) |

## 8. Documents
Various related documents.

### 8.1 ISS by Olivier Rt
Comparison of the real ISS and the [set 21321-1](#SET21321-1) by Olivier Rt:

[https://olivierrt.wordpress.com/2020/10/20/lego-iss-21321-vs-the-real-international-space-station/](https://olivierrt.wordpress.com/2020/10/20/lego-iss-21321-vs-the-real-international-space-station/)

### 8.2 ISS by Riccardo Russo et al.
Elements of the [set 21321-1](#SET21321) to the real ISS with history by Riccardo Russo, Luca Peccatori, Johan Broman and Zovits Ádám.

[Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/permalink/907131853025400/), [Youtube video](https://youtu.be/ZBIZI4w7FL8)

### 8.3 Rudi Landmann's modding notes on KingsKnight's shuttle
How to mod [Kingsknight's 1:110 Space Shuttle](#MOCkingsknightspaceshuttle) to represent specific orbiters at
specific points in their histories. A companion document
for the [current 2020 version](#MOCkingsknightspaceshuttlev2) is coming.

[Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/permalink/1021285918276659/)

### 8.4 Stickers and decals
<div class="COMimg">
Various materials have been tested for home made stickers and decals. [Experience and results are shown in this document.](imgCOM/sticker_comparison.pdf)
[![COMstickers](imgCOM/COMstickers.jpg)](imgCOM/sticker_comparison.pdf "sticker_comparison.pdf")
</div>

### 8.5 Overview of Energia family
Energia family by Allan Jordan, with help of Sunder59, KingsKnight, David Welling and Sebastian Schoen.
The family consists of several modules that allow interchangeable construction.

<div class="COMimg">
[![DOCenergia](DOC/DOCenergia.jpg)](DOC/DOCenergia_large.jpg)
</div>

On the [figure](https://www.facebook.com/groups/legospacebuilds/posts/1061635147575069/) from left:

1. [Vulkan Hercules (Variant 2c)](#MOCallanjordanvulkanherculesv2c)     <!-- rebrickable id 82765 -->
1. [Vulkan Hercules (Variant 2b)](#MOCallanjordanvulkanherculesv2b)     <!-- rebrickable id 82763 -->
1. [Vulkan Hercules (Variant 2a)](#MOCallanjordanvulkanherculesv2a)     <!-- rebrickable id 82762 -->
1. [Vulkan Hercules (Variant 1)](#MOCallanjordanvulcanhercules)         <!-- rebrickable id 82760 -->
1. [Vulkan](#MOCallanjordanvulcan)                                      <!-- rebrickable id 82759 -->
1. [MTK-VP (Later variant)](#MOCallanjordanmtkvpv2)                     <!-- rebrickable id 82758 -->
1. [MTK-VP (Earlier variant)](#MOCallanjordanmtkvpv1)                   <!-- rebrickable id 82757 -->
1. [Energia II / Uragan (Variant 2)](#MOCallanjordanuragan3)            <!-- rebrickable id 80555 -->
1. [Energia II / Uragan (Variant 1, early)](#MOCallanjordanuragan1)     <!-- rebrickable id 82302 -->
1. [Energia-Buran](#MOCallanjordanburan)                                <!-- rebrickable id 80043 -->
1. [OK-92](#MOCallanjordanok92)                                         <!-- rebrickable id 82299 -->
1. [OS-120](#MOCallanjordanos120)                                       <!-- rebrickable id 82298 -->
1. [Energia-Polyus (Flown)](#MOCallanjordanpolyus)                      <!-- rebrickable id 80644 -->
1. [Energia-Polyus (Early variant)](#MOCallanjordanpolyusvariant2)      <!-- rebrickable id 81893 -->
1. [Buran-T (Variant 2)](#MOCallanjordanburantv2)                       <!-- rebrickable id 81892 -->
1. [Buran-T (Variant 1)](#MOCallanjordanburant)                         <!-- rebrickable id 81891 -->
1. [Energia](#MOCallanjordanenergia)                                    <!-- rebrickable id 80642 -->
1. [Energia-M (Built)](#MOCallanjordanenergiamv2)                       <!-- rebrickable id 81877 -->
1. [Energia-M (Early design, proposed)](#MOCallanjordanenergiam)        <!-- rebrickable id 81881 -->

Up to it there is also: [Buran atmospheric test vehicle OK-GLI](#MOCallanjordanburanokgli), [Buran only MOC](#MOCallanjordanburanonly), [Polyus only MOC](#MOCallanjordanpolyusonly). 
Old design of Energia Polyus by [Sunder59](#MOCsunder59energiapolyus), improved design by [MoppeW40k](#MOCmoppew40kenergiapolyus) and follow up design by [Florent Todeschini](#MOCflorenttodeschinienergiapolyus).

### 8.6 Apollo Moon mission chart
By Eiffleman:

Chart showing the various stages of the Apollo Moon mission. This is based on an original 1967 NASA
drawing. I have replaced the step illustrations with images of the Lego Saturn V set. Rendered with
printed LM and CM parts, thanks to Adam Wilde and Greg Kremer. 

You can buy the the poster from <a href="https://astrography.com/products/apollo-flight-plan-lego?_pos=1&_sid=4886f7888&_ss=r">Astrography</a>.

[Version 14000x4600 pixel size](https://www.facebook.com/groups/legospacebuilds/permalink/1042143572857560/)

[Version 7000x2300 pixel size](https://www.facebook.com/groups/legospacebuilds/permalink/1039545769784007/)

<div class="COMimg">
![DOCapollomissionchart](DOC/DOCapollomissionchart.jpg)
</div>

### 8.7 Dimensions of cylinders 
[Nicolas Riveau](#MOCnicholasriveaucylinders) made a nice collection of cylinder building techniques. This image shows the dimensions of the cylinders.

Diameters were measured by rendering an image and fitting a circle into the cylinder. Click the image to show large uncompressed version (4.6 MB png!).
[Link to the svg source](DOC/DOCcylinders_dimensions.svg) (use e.g. Inkscape to edit).

<div class="COMimg">
[![DOCcylinders_dimensions](DOC/DOCcylinders_dimensions.jpg)](DOC/DOCcylinders_dimensions.png)
</div>

### 8.8 LEGO cutaway Saturn V
By Eiffleman:

The original is a 1967 Boeing drawing of a cut away Saturn V. I have rendered a
cut-away of the set. I have removed one of the rows of Taps from the LES, the
set's tower is too tall- Its the one noticable error with the set and stood out
when I overlaid the render on the original drawing.

[2536x4096 jpg file at Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/permalink/1250497792022136/)

<div class="COMimg">
![COMapollomissionchart](DOC/DOCsaturnvcutaway.jpg)
</div>

## 8.9 Guides with modifications for set 21321
[Facebook 'guides'](https://www.facebook.com/groups/legospacebuilds/learning_content/?filter=1065793917386026&post=843590646566148)
with list of modifications for set 21321. Contains modifications posted in
[Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds).

### Guide 1: Modifications to official set 21321 - International Space Station
Guide contains following mods:
- [Luca Peccatori - compilation of mods](#MOClucapeccatoriisscompilation)
- [Luca Peccatori - ROSA panels](#MOCpeccatoriirosapanels)
- [Damian Bailey - Shuttle stand](#MOCdamianbaileyshuttlestand)
- [Eiffleman - Axiom](#MOCeifflemanaxionspace)
- [DamoB - Cygnus](#MOCdamobcygnus)
- [Adam Wilde - Soyuz, Progress](#MOCadamwildesoyuzprogressimprovement)
- [Adam Wilde - Dextre](#MOCadamwildedextre)
- [Eiffleman - solar panels, radiators](#MOCeifflemanisssolarpanelradiatormod)
- [Luca Peccatori - russian segment](#MOCpeccatoriissrussiansegment)
- [Nicholas Tucker - russian segment](#MOCnicholastuckerissimprovedrussiansegment)

### Guide 2: 1:220 Shuttle to accompany official set 21321 - ISS
Guide contains following shuttles:
- [Damian Bailey](#MOCdamobspaceshuttle)
- [Adam Wilde](#MOCadamwildespaceshuttle)
- [Dan Fallon](#MOCdanfallonspaceshuttle)
- [Eiffleman](#MOCeifflemanspaceshuttle)

## 8.10 Soyuz 7K development tree
Soyuz 7K family by NightHawk11991.

<div class="COMimg">
[![DOCsoyuz7kfamilytree](DOC/DOCsoyuz7kfamilytree.png)](DOC/DOCsoyuz7kfamilytree_large.png)
</div>

   |   |   |  
---|---|---|---
&nbsp; | &nbsp; | [7K / Soyuz-A](#MOCnighthawk119917k) |
&nbsp; | [7K-OK (A)](#MOCnighthawk119917koka)<br> [7K-OK (P)](#MOCnighthawk119917kokp) | &nbsp; | [Soyuz P](#MOCnighthawk11991soyuzp)<br> [Soyuz R](#MOCnighthawk11991soyuzr)<br> [Soyuz-PPK](#MOCnighthawk11991soyuzppk)<br> [Soyuz-VI](#MOCnighthawk11991soyuzvi)<br> [Soyuz-VI OIS](#MOCnighthawk11991soyuzviois)
[7K-OKS](#MOCnighthawk119917koks) | [7K-LOK](#MOCnighthawk119917klok) | [7K-L1](#MOCnighthawk119917kl1) | [Soyuz-T](#MOCnighthawk11991soyuzt)
[7K-T](#MOCnighthawk119917kt) | [7K-TM](#MOCnighthawk119917ktm) | &nbsp; | [Soyuz-TM](#MOCnighthawk11991soyuztm)
 &nbsp; | &nbsp; | &nbsp; | [Soyuz-TMA](#MOCnighthawk11991soyuztma)

## 9. Comparisons
Because some rockets or spacecrafts are represented in several MOCs, I was curious which represents the real rocket best. I hope no one gets offended!

### 9.1 A4, 1:110 scale
Models by [MuscoviteSandwich](#MOCmuscovitesandwichv2), [Mark Balderama](#MOCmarkbalderramaa4), and [Kaero](#MOCkaeroa4withvehicles) without and with decals.
<div class="COMimg">
![COMa4](imgCOM/COMa4.jpg "COMa4")
</div>
(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).
<!-- add MOClucapeccatoriv2 -->
<!-- add MOCchhaa4 -->

### 9.2 Space Shuttle
<div class="COMimg">
This comparison is by KingsKnight from Lego Ideas page.
From left: real Space Shuttle, [KingsKnight](#MOCkingsknightspaceshuttle), [Lego set 10231](#SET10231-1).
![COMspaceshuttle](imgCOM/COMspaceshuttle.jpg "COMspaceshuttle")
</div>  

.

Comparison by Tony Bela. [International Space Station](#SET21321-1), [Shuttle Transporter](#SET31091-1).
<div class="COMimg">
![COMspaceshuttleiss](imgCOM/COMspaceshuttleiss.jpg "COMspaceshuttleiss")
</div>

### 9.3 Launch Umbilical Towers, 1:110 scale
<div class="COMimg">
**Angle comparison**. Pictures made by Wolfram Broszies. Click the image to show large size image (7.7 MB jpg!).
From left: Real LUT, [Bailey Fullarton](#MOCbaileyfullartonlut), [Joseph Chambers](#MOCjosephchamberslut), [Michael Cameron](#MOCmichaelcameronlut), [Nathan Readioff](#MOCnathanrlut), [Valerie Roche, Greg Kremer (version 10)](#MOCrochekremerlut), [Janotechnic](#MOCjanotechniclut).
[![COMluts](imgCOM/COMluts.jpg)](imgCOM/COMluts_large.jpg "COMluts")
</div>

.

<div class="COMimg">
**Side comparison**. Height in feet. Renders made by [Markus Vitzethum](https://www.facebook.com/groups/legospacebuilds/posts/1161866197551963/). 
Click the image to show large size image (10.8 MB png!).

Important insight by Nathan Readioff:
*One of the problems with the Saturn V (when built out of box, with no modifications) is that the various stages are slightly stretched or compressed vertically so that the heights of each stage are a whole number of studs. This means that while the rocket is overall 1:110 scale, individual stages are not. Also, the rocket scale varies from 1:110 to 1:112, depending on whether you measure the rocket's width or height. (1:112 is actually more correct when you correct the LES by shrinking the height by two bricks) All of this must be considered when designing an LUT, which leads to the following design choices: 1) allow each LUT tower level to be the same vertical height. The service arms must then be placed so that a) they touch the right part of the rocket but are not aligned to the correct vertical position on the tower or b) place the arms at the right vertical position on the tower and leave them misaligned on the rocket (this is really noticeable with the crew access arm being a brick or two above the Command Module). Or 2) Change the height of each tower level so that the service arms align correctly with the rocket and the tower. I went with option 2 for my LUT, but this leaves the entire tower 2 bricks too short if I remember right.*

*Another challenge with scale comes from the tapering structure on the lowest tower levels (L0-L80), because this requires the construction of triangles that are “in system”, with a side length that is a whole number of studs. A perfect reproduction is geometrically impossible, and this affects the relative heights of the lower levels and the placement of Service Arm 1. All these different possibilities mean there is no “definitive” LUT design. Jo Chambers model is by far the most accurate, but it is designed for the unmodified Saturn V, and has all vertical positions (levels, arms, etc) adjusted accordingly.*

From left: [Nathan Readioff](#MOCnathanrlut), [Valerie Roche, Greg Kremer](#MOCrochekremerlut), [Michael Cameron](#MOCmichaelcameronlut), [Joseph Chambers](#MOCjosephchamberslut), LUT schematics ([apollomaniacs](https://www.apollomaniacs.com/apollo/mobile_launcher_en.htm)), Real LUT (NASA, Apollo 11).
[![COMlutsside](imgCOM/COMlutsside.jpg)](imgCOM/COMlutsside_large.png "COMlutsside")
</div>

### 9.4 Soyuz
<div class="COMimg">
Various soyuz rockets. From left: [suchshibeinu](#MOCsuchshibeinusoyuz), [tech_niek](#MOCtechnieksoyuzrocket), [endrega](#MOCendregasoyuz), [Andor Schindler](#MOCandorschindlersoyuz21v), [Andor Schindler](#MOCandorschindlersoyuzfg), [Stania](#MOCstaniasoyuz).
Click the image to show large one.
[![COMsoyuz](imgCOM/COMsoyuz.jpg)](imgCOM/COMsoyuz_large.jpg "COMsoyuz")
</div>
(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).
<!-- Missing: MOCskysaacsoyuzfgms MOCskysaacsoyuzfgprogress MOCskysaacvostok -->

### 9.5 Falcon and Falcon Heavy
<div class="COMimg">
Falcon and Falcon heavy rockets in 1:110 scale. 
From left: [RepRage](#MOCrepragefalcon9), [endrega](#MOCendregaspacexfalcon9), [curtisdf](#MOCcurtisdffalcon9), [Xael](#MOCxaelsootyblockvfalcon9crewdragon), [? at AB's Google drive](#MOCXfalconheavy), [curtisdf](#MOCcurtisdffalconheavy), [Eiffleman](#MOCeifflemanfalconheavy), [endrega](#MOCendregaspacexfalconheavy), [Youri Hamon](#MOCyourihamonspacexfalconheavy).
Click the image to show large one.
[![COMfalcon](imgCOM/COMfalcon.jpg)](imgCOM/COMfalcon_large.jpg "COMfalcon")

<!-- Should be added: MOCjeremyworlockfalcon9block5 XXX -->
<!-- Should be added: muscovitesandwich! moc. and update its picture!!! XXX -->
<!-- SHould be added: MOCnicvermeerenfalconheavy XXX -->
<!-- MOCthebrickfrontierfalcon9      -->
<!-- MOCthebrickfrontierfalcon9sooty -->

</div>
(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).

### 9.6 Titan II Gemini Launch Vehicle
<div class="COMimg">
Titan II GLV rockets in 1:110 scale.
From left:
[SpaceXplorer 16](#MOCspacexplorertitanglv), [Gilles Nawrocki's modification](#MOCgillesnawrockititaniiglvpad), [Kevin Huang, bedrockblaze135, Dan Fallon](#MOCkevinhuangtitaniiglv), [MuscoviteSandwich](#MOCmuscovitesandwichgeminititan), [legorockets](#MOClegorocketsgeminititan), [Joel Denning's](#MOCjoeldenninggeminititanmod) and [luxordeathbed's](#MOCluxordeathbedgeminitianalternative) modifications, [Eiffleman](#MOCeifflemangeminititan).
Click the image to show large one.
[![COMgeminititan](imgCOM/COMgeminititan.jpg)](imgCOM/COMgeminititan_large.jpg "COMgeminititan")
</div>
(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).
<!-- add MOCmuscovitesandwichdynasoar -->

### 9.7 H-II Transfer Vehicle
<div class="COMimg">
H-II Transfer Vehicles in 1:110 scale. MOCs depicts different HII-TV missions with different exposed paletes (load).
From left:
[MoppeW40k](#MOCmoppew40khtv), [Dan Fallon](#MOCdanfallonhtv), [WillM](#MOCwillmhtv), [Kaero](#MOCkaerohtv).
Click the upper image to show large one.
[![COMhiitv](imgCOM/COMhiitv.jpg)](imgCOM/COMhiitv_large.jpg "COMhiitv")
![COMhiitvangle](imgCOM/COMhiitvangle.jpg "COMhiitvangle")
</div>

(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).

### 9.8 Saturn IB
<div class="COMimg">
Saturn IB in 1:110 scale. MOCs depicts different missions.
From left:
[Brandon Crain](#MOCbrandoncrainsaturn1b), [David Welling](#MOCdavidwells44saturn1bseries), [Eiffleman](#MOCeifflemansaturn1b), [legorockets](#MOClegorocketssaturn1B), [Nathan Readioff](#MOCnathanrsaturn1b), Adam Wilde (instructions not yet released publicly).
Photos are by NASA, public domain. Click the image to show large one (14 MB png!).
[![COMsaturnib](imgCOM/COMsaturnib.jpg)](imgCOM/COMsaturnib_large.png "COMsaturnib")

</div>

(This comparison was made by myself at time I was selecting MOC to build. Newer and charged MOCs can be missing. I do not
have time to update comparison, if you want to do it, contact me and I will send you base files and help you to update this comparison).

## 10. Various interresting links
- [Real Cubesat from LEGO](https://amsat-uk.org/tag/lego-cubesat/?fbclid=IwAR3BJMnqKftP6bgb8bpMPOaCOxclNLRYNlRNNDRBRH1rlZ3FVRAhFyHo0ss). Was it launched? If someone knows more info, please send me links>
- [Here was a LEGO model of Planet Dove 3U cubesat](https://hobbyspace.com/Blog/?p=16610). Links are broken, does the instructions exist anywhere?
- [ESA video: Using LEGO to simulate ESA's touchdown on a comet](https://www.youtube.com/watch?v=1oEaGjgOB0M).
- [ESA video: LEGO in space mission operations](https://www.youtube.com/watch?v=lKi3Y_Qid7w).
- [Rosetta Lander Education Kit](https://www.esa.int/Space_in_Member_States/Czech_Republic/Pronasledovatel_komety_ESA_Rosetta_vstoupil_do_sveta_LEGO_R). Is the kit available somwhere?

## 11. Honorary mentions
**Bob Turner**
[Red Dwarf Lego](https://ideas.lego.com/projects/476ec4f8-d2a4-44d2-b70c-e67a2256d20d) - [The instructions for the Red Dwarf LEGO will be posted on this website on August 2020](https://www.bobsvintagebricks.com/reddwarflego)

**TheMugbearer**
X [FTL: Faster Than Light](https://ideas.lego.com/projects/714583f1-17c8-455a-969f-d5f9f9be42d1)

## 12. History of changes
Git log ouput can be found on [Gitlab project site](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/commits/master).

---

<a href='http://www.freevisitorcounters.com'>freevisitorcounters</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=4812884542147d964c56cc503265b2bd9635c0a9'></script>
<script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/755066/t/4"></script>
<script src="table_data.js" type="application/javascript"></script>
<script src="column_data.js" type="application/javascript"></script>
<script src="filter_table.js" type="application/javascript"></script>
<!-- Script for lazy loading of images. script copied from https://github.com/verlok/vanilla-lazyload -->
<!-- author is Andrea Verlicchi, aka verlok. MIT license. -->
<script src="lazyload.min.js" type="application/javascript"></script>
<!-- Initialization of lazyload script: -->
<script>
var lazyLoadInstance = new LazyLoad({
  // Your custom settings go here
});
</script>
